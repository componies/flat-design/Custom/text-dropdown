(($) => {
  'use strict';

  Drupal.behaviors.dropdown = {
    attach: function(context, settings) {
      $('.text-dropdown__toggle', context).once('text-dropdown').click(function (e) {
        $(this).next('.text-dropdown__content').slideToggle();
        $(this).toggleClass('js-open');
       
        if ($(this).hasClass('js-open')) {
          $(this).attr("aria-expanded", true);
        } else {
          $(this).attr("aria-expanded", false);
        }
      });
    }
  };

})(jQuery);
